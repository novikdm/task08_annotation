package com.novikdm;

import com.novikdm.view.ConsoleMenu;

import java.io.IOException;

public class Application {
  public static void main(String[] args) {
    ConsoleMenu menu = new ConsoleMenu();
    menu.show();
  }
}
