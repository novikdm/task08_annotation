package com.novikdm.controller;

import com.novikdm.model.FieldAnnotation;
import com.novikdm.model.Model;
import com.novikdm.model.taskmodels.Student;
import com.novikdm.view.ConsoleMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.Arrays;


public class ViewControllerImpl implements Controller {
  private Model model;
  private static Logger print = LogManager.getLogger(ConsoleMenu.class);

  public void annotationTask(Model student) {
    if (student ==  null) {
      model = new Student();
    }
    Field [] field = model.getClass().getDeclaredFields();
    for (Field f:
         field) {
      if(f.isAnnotationPresent(FieldAnnotation.class)) {
        FieldAnnotation annotation = f.getAnnotation(FieldAnnotation.class);
        String param = annotation.param();
        boolean b = annotation.stringTrigger();
        print.info("Field: " + param + "; " + "StringTrigger: " + b);
      }
    }
  }


}
