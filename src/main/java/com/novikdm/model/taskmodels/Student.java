package com.novikdm.model.taskmodels;

import com.novikdm.model.FieldAnnotation;
import com.novikdm.model.Model;
import com.novikdm.model.MyFirstAnnotation;

import java.util.Objects;


public class Student implements Model {
  @FieldAnnotation(param = "Name")
  private String name;
  @FieldAnnotation(param = "Age", stringTrigger = false)
  private int age;
  @FieldAnnotation(param = "Gender", stringTrigger = false)
  private boolean gender;
  @FieldAnnotation(param = "Hostel", stringTrigger = false)
  private int hostelNumber;

  @MyFirstAnnotation()
  public Student() {
    this.name = this.getClass()
            .getConstructors()[0]
            .getAnnotation(MyFirstAnnotation.class)
            .name();
    this.age = this.getClass()
            .getConstructors()[0]
            .getAnnotation(MyFirstAnnotation.class)
            .age();
    this.gender = this.getClass()
            .getConstructors()[0]
            .getAnnotation(MyFirstAnnotation.class)
            .gender();
    this.hostelNumber = 0;
  }

  public Student(String name, int age, boolean gender, int hostelNumber) {
    this.name = name;
    this.age = age;
    this.gender = gender;
    this.hostelNumber = hostelNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public boolean isGender() {
    return gender;
  }

  public void setGender(boolean gender) {
    this.gender = gender;
  }

  public int getHostelNumber() {
    return hostelNumber;
  }

  public void setHostelNumber(int hostelNumber) {
    this.hostelNumber = hostelNumber;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Student student = (Student) o;
    return age == student.age &&
            gender == student.gender &&
            hostelNumber == student.hostelNumber &&
            Objects.equals(name, student.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, age, gender, hostelNumber);
  }

  @Override
  public String toString() {
    return "Student{" +
            "name='" + name + '\'' +
            ", age=" + age +
            ", gender=" + gender +
            ", hostelNumber=" + hostelNumber +
            '}';
  }
}
