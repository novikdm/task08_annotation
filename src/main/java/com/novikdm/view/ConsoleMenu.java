package com.novikdm.view;

import com.novikdm.controller.Controller;
import com.novikdm.controller.ViewControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleMenu {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private Scanner inputLine = new Scanner(System.in);
  private static Logger print = LogManager.getLogger(ConsoleMenu.class);

  public ConsoleMenu() {
    controller = new ViewControllerImpl();
    methodsMenu = new LinkedHashMap<>();
    menu = new LinkedHashMap<>();
    menu.put("1", "| 1 Annotation task |");
    menu.put("0", "| 0 Exit |");
    methodsMenu.put("1", this::firstTask);
  }

  private void firstTask() {
    controller = new ViewControllerImpl();
    print.info("----------------");
    ((ViewControllerImpl) controller).annotationTask(null);
  }

  private String getInputText() {
    String text = "";
    while (true) {
      String line = inputLine.nextLine();
      if (line.equals("")) {
        break;
      } else {
        text = text + line + " ";
      }
    }
    return text;
  }

  private void outputMenu() {
    print.trace("MENU:");
    for (String str : menu.values()) {
      print.trace(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      print.trace("Please, select menu point.");
      keyMenu = inputLine.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("0"));
  }
}
