package com.novikdm.view;

@FunctionalInterface
public interface Printable {
  void print();
}
